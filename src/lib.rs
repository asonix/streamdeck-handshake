use rand::prelude::ThreadRng;
use std::{borrow::Cow, time::Duration};
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};
use tokio_serial::{SerialPortBuilderExt, SerialStream};
use tracing_error::SpanTrace;

/// Errors produced by streamdeck-handshake.
///
/// This type includes a [`SpanTrace`] for clearer integration into the Tracing ecosystem
///
/// [`SpanTrace`]: https://docs.rs/tracing-error/0.1.2/tracing_error/struct.SpanTrace.html
#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
    context: SpanTrace,
}

impl Error {
    /// Inspect the kind of the error
    pub fn kind(&self) -> &ErrorKind {
        &self.kind
    }
}

/// Errors produced by streamdeck-handshake.
///
/// This can be used to inspect the origin of the error, whether it's a failure in the handshake
/// itself or some other kind
#[derive(Debug, thiserror::Error)]
pub enum ErrorKind {
    #[error(transparent)]
    Io(#[from] std::io::Error),

    #[error(transparent)]
    Serial(#[from] tokio_serial::Error),

    #[error(transparent)]
    Rand(#[from] rand::Error),

    #[error("Client failed the handshake")]
    HandshakeError,
}

impl ErrorKind {
    /// Check whether the error is a handshake failure or a more serious issue
    pub fn is_handshake_failure(&self) -> bool {
        matches!(self, Self::HandshakeError)
    }
}

/// Perform the handshake operation
pub async fn handshake(path: Cow<'_, str>) -> Result<SerialStream, Error> {
    handshake_inner::<_, ThreadRng>(path).await
}

const INPUT_LEN: usize = 16;

trait OpenPort: AsyncRead + AsyncWrite {
    fn open(path: Cow<'_, str>) -> Result<Self, Error>
    where
        Self: Sized;
}

trait OpenRand {
    fn open() -> Self
    where
        Self: Sized;

    fn try_fill(&mut self, buf: &mut [u8]) -> Result<(), Error>;
}

impl OpenPort for SerialStream {
    fn open(path: Cow<'_, str>) -> Result<Self, Error> {
        tokio_serial::new(path, 9600)
            .timeout(Duration::from_millis(500))
            .open_native_async()
            .map_err(From::from)
    }
}

impl OpenRand for ThreadRng {
    fn open() -> Self {
        rand::thread_rng()
    }

    fn try_fill(&mut self, buf: &mut [u8]) -> Result<(), Error> {
        use rand::Fill;
        buf.try_fill(self)?;
        Ok(())
    }
}

async fn handshake_inner<Port, Rand>(path: Cow<'_, str>) -> Result<Port, Error>
where
    Port: OpenPort + Unpin,
    Rand: OpenRand,
{
    let mut output = [0u8; INPUT_LEN * 2];
    let mut input = [0u8; INPUT_LEN];

    let mut port = Port::open(path)?;

    Rand::open().try_fill(&mut output)?;
    tracing::debug!("Writing {:?}", output);
    port.write_all(&output).await?;
    port.flush().await?;
    tracing::debug!("Flushed");

    port.read_exact(&mut input).await?;
    tracing::debug!("Read {:?}", input);

    for (index, byte) in input.iter().enumerate() {
        if *byte != (output[index] ^ output[index + INPUT_LEN]) {
            return Err(ErrorKind::HandshakeError.into());
        }
        output[index] = *byte;
        output[index + INPUT_LEN] = *byte;
    }

    tracing::debug!("Calculated {:?}", output);
    port.write_all(&output).await?;
    port.flush().await?;
    tracing::debug!("Flushed");

    Ok(port)
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{}", self.kind)?;
        std::fmt::Display::fmt(&self.context, f)
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        self.kind.source()
    }
}

impl<T> From<T> for Error
where
    ErrorKind: From<T>,
{
    fn from(error: T) -> Self {
        Error {
            kind: error.into(),
            context: SpanTrace::capture(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{handshake_inner, Error, OpenPort, OpenRand, INPUT_LEN};
    use std::borrow::Cow;
    use tokio::io::{AsyncRead, AsyncWrite};

    #[tokio::test]
    async fn test_handshake() {
        let port = handshake_inner::<FakePort, FakeRand>("".into())
            .await
            .unwrap();

        // writes INPUT_LEN * 2 twice
        assert_eq!(port.written, INPUT_LEN * 4);
        // reads INPUT_LEN once
        assert_eq!(port.read, INPUT_LEN);
    }

    #[tokio::test]
    async fn fail_handshake() {
        let err = handshake_inner::<FakePort, BadRand>("".into())
            .await
            .unwrap_err();

        assert!(err.kind().is_handshake_failure());
    }

    #[derive(Debug)]
    struct FakePort {
        written: usize,
        read: usize,
    }

    impl AsyncRead for FakePort {
        fn poll_read(
            mut self: std::pin::Pin<&mut Self>,
            _: &mut std::task::Context<'_>,
            buf: &mut tokio::io::ReadBuf<'_>,
        ) -> std::task::Poll<std::io::Result<()>> {
            let to_write = buf.remaining().min(INPUT_LEN);

            let mut v = vec![0u8; to_write];

            for (i, elem) in v[0..to_write].iter_mut().enumerate() {
                let index = self.read + i;

                *elem = (index ^ index + INPUT_LEN) as u8;
            }

            self.read += to_write;

            buf.put_slice(&v[0..to_write]);
            std::task::Poll::Ready(Ok(()))
        }
    }

    impl AsyncWrite for FakePort {
        fn poll_write(
            mut self: std::pin::Pin<&mut Self>,
            _: &mut std::task::Context<'_>,
            buf: &[u8],
        ) -> std::task::Poll<Result<usize, std::io::Error>> {
            self.written += buf.len();
            std::task::Poll::Ready(Ok(buf.len()))
        }

        fn poll_flush(
            self: std::pin::Pin<&mut Self>,
            _: &mut std::task::Context<'_>,
        ) -> std::task::Poll<Result<(), std::io::Error>> {
            std::task::Poll::Ready(Ok(()))
        }

        fn poll_shutdown(
            self: std::pin::Pin<&mut Self>,
            _: &mut std::task::Context<'_>,
        ) -> std::task::Poll<Result<(), std::io::Error>> {
            std::task::Poll::Ready(Ok(()))
        }
    }

    impl OpenPort for FakePort {
        fn open(_: Cow<'_, str>) -> Result<Self, Error>
        where
            Self: Sized,
        {
            Ok(FakePort {
                written: 0,
                read: 0,
            })
        }
    }

    struct FakeRand;

    impl OpenRand for FakeRand {
        fn open() -> Self
        where
            Self: Sized,
        {
            FakeRand
        }

        fn try_fill(&mut self, buf: &mut [u8]) -> Result<(), Error> {
            for (i, byte) in buf.iter_mut().enumerate() {
                // fill with 1
                *byte = i as u8;
            }
            Ok(())
        }
    }

    struct BadRand;

    impl OpenRand for BadRand {
        fn open() -> Self
        where
            Self: Sized,
        {
            BadRand
        }

        fn try_fill(&mut self, buf: &mut [u8]) -> Result<(), Error> {
            for byte in buf.iter_mut() {
                *byte = 1;
            }
            Ok(())
        }
    }
}
